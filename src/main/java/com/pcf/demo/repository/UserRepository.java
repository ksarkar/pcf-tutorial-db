package com.pcf.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.pcf.demo.data.User;

public interface UserRepository extends CrudRepository<User, Integer>{

}
