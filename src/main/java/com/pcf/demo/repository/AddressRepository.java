package com.pcf.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.pcf.demo.data.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {

}
