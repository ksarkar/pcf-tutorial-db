package com.pcf.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pcf.demo.data.User;
import com.pcf.demo.repository.UserRepository;


@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepo;
	
	@PostMapping(path = "/users")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		//user.addAddresses(user.getAddresses());
		User savedUser = userRepo.save(user);
		
		return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
	}
	
	@GetMapping(path = "/users")
	public List<User> findAllUsers() {
		return (List<User>) userRepo.findAll();
	}
	
	@GetMapping(path = "/user/{userId}")
	public ResponseEntity<User> findUserById(@PathVariable(value = "userId") Integer userId) {
		Optional<User> result=  userRepo.findById(userId);
		if(result.isPresent()) {
			return new ResponseEntity<User>(result.get(), HttpStatus.FOUND);
		}
		
		return new ResponseEntity<User>( HttpStatus.NOT_FOUND);
	}
}

